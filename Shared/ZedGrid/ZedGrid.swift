import SwiftUI

struct ZedGrid: View {
    
    @State var model = ZedGridModel()
    
    var body: some View {
        
        VStack {
            ForEach(model.rows) { row in
                HStack {
                    ForEach(row.cells) { cell in
                        ZedCell(activeCell: $model.activeCell, cell: cell).zIndex(model.getZ(cell: cell))
                    }
                }.zIndex(model.getZRow(row: row))
            }
        }
  
    }
    
}
