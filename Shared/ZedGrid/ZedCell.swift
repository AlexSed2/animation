import SwiftUI


struct ZedCell: View {
    
    @State var model = CellModel()
    @Binding var activeCell: ZedGridModel.Cell
    let cell: ZedGridModel.Cell
    
    var body: some View {
        
        let drug = DragGesture().onChanged { value in
            model.translation = value.translation.diagonalFloat
            model.startTranslation = value.startLocation.simd2float
        }.onEnded { _ in
            withAnimation(Animation.spring(response: 0.55, dampingFraction: 0.525, blendDuration: 0.5)) {
                model.translation = .zero
            }
        }
        
        let k = Blank()
            .frame(width: model.size.width, height: model.size.height)
            .scaleEffect(x: 1 + model.scale.x, y: 1 + model.scale.y, anchor: model.anchor)
            .gesture(drug)
            .onTouchDownGesture {
                activeCell = cell
            }
        return k
        
    }

}


