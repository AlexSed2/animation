import SwiftUI
import simd

struct ZedCellModel: Scaleble {
    var diagonal: SIMD2<Float> = [200, 150]
    var offset: SIMD2<Float> = .zero
    var translation: SIMD2<Float> = .zero
    var startTranslation: SIMD2<Float> = .zero
}


struct ZedGridModel {
    struct Cell: Equatable, Identifiable {
        let id: Int
    }
    struct Row: Equatable, Identifiable {
        let cells: [Cell]
        var id: Int { cells.reduce(0) {  $0 + $1.id  } }
    }
    let rows: [Row]
    var activeCell: Cell
    init() {
        var rows = [[Cell]]()
        var row = [Cell]()
        row.append(Cell(id: 0))
        row.append(Cell(id: 1))
        rows.append(row)
        row = []
        row.append(Cell(id: 2))
        row.append(Cell(id: 3))
        rows.append(row)
        self.rows = rows.map { Row(cells: $0) }
        activeCell = row.last!
    }
    func getZ(cell: Cell) -> Double {
        cell == activeCell ? 1 : 0
    }
    func getZRow(row: Row) -> Double {
        row.cells.contains(activeCell) ? 1 : 0
    }
}
