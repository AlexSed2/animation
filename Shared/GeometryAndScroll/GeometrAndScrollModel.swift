import CoreGraphics

struct GeomCellModel: Scaleble {
    
    var diagonal: SIMD2<Float> = [300, 250]
    var offset: SIMD2<Float> = .zero
    var translation: SIMD2<Float> = .zero
    var startTranslation: SIMD2<Float> = .zero
}

struct GeomGridModel {
    var current = GeomCellModel()
    var currentFrame = CGRect.zero {
        didSet {
//            print("origin: \(currentFrame.origin)")
        }
    }
}
