import SwiftUI


struct PlainGeomCell: View {
    
    let cell: Square
    @Binding var outerRects: RectHoleder
    @State var model = CellModel()
    
    var body: some View {
        
        let drug = DragGesture().onChanged { value in
            withAnimation(.easeOut) {
                model.translation = value.translation.diagonalFloat
                model.startTranslation = value.startLocation.simd2float
            }
        }.onEnded { _ in
            withAnimation(Animation.spring()) {
                model.translation = .zero
                withAnimation {}
            }
        }
        
        let k = Blank()
            .frame(width: model.size.width, height: model.size.height)
            .background(
                GeometryReader { geometry in
                    check(k: Rectangle().fill(Color.gray).cornerRadius(5), geometry: geometry)
                })
            .scaleEffect(x: 1 + model.scale.x, y: 1 + model.scale.y, anchor: model.anchor)
            .gesture(drug)
        return k
        
    }
    
    func check<K: View>(k: K, geometry: GeometryProxy) -> some View {
        DispatchQueue.main.async {
            outerRects.rects[cell] = geometry.frame(in: .named("Z"))
        }
        return k
    }
}
