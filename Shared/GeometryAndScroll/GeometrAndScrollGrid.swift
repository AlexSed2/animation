import SwiftUI

extension GeooetryAndScrollGridView {
    struct Model {
        var fridge = DynamicCanvas()
        var isShown: Bool = false
    }
}


struct GeooetryAndScrollGridView: View {
    
    
    @State var model = Model()
    @State var rects = RectHoleder()
    
    var body: some View {
        
        let v =
        VStack {
            ForEach(model.fridge.rows) { row in
                HStack {
                    ForEach(row) { element in
                        PlainGeomCell(cell: element, outerRects: $rects)
                    }
                }
            }
        }
        
        
        
        
        let z = ZStack(alignment: .topLeading) {
            v
            
            Rectangle().fill(Color.orange)
                .frame(width: 50, height: 50)
                .offset(x: rects[model.fridge.rows[0][0]].origin.x, y: rects[model.fridge.rows[0][0]].origin.y)
            
            Rectangle().fill(Color.orange)
//                .frame(width: rects[model.fridge.rows[1][0]].width, height: rects[model.fridge.rows[1][0]].height)
                .frame(width: 50, height: 50)
                .scaleEffect(2.2, anchor: .topLeading)
                .offset(x: rects[model.fridge.rows[1][0]].origin.x, y: rects[model.fridge.rows[1][0]].origin.y)
                
            
            Rectangle().fill(Color.orange)
                .frame(width: 50, height: 50)
                .offset(x: rects[model.fridge.rows[0][1]].origin.x, y: rects[model.fridge.rows[0][1]].origin.y)
            
        }
            .coordinateSpace(name: "Z")
            .border(Color.black, width: 1)
            
            
        let s = ScrollView {
            z
        }
        
        return s

    }
}
