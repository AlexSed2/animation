import SwiftUI
import Algorithms

public struct Square: Equatable, Hashable, Identifiable, Comparable {
    let name: String
    var uimodel: Cell.Model
    public var id: String { name }
    public static func == (lhs: Square, rhs: Square) -> Bool {
        lhs.name == rhs.name
    }
    public func hash(into hasher: inout Hasher) {
        hasher.combine(name)
    }
    public static func < (lhs: Square, rhs: Square) -> Bool {
        lhs.name < rhs.name
    }
}

struct DynamicStack {
    var items: Set<Square>
    init() {
        let a = (0..<9).map { Square(name: "\($0)", uimodel: Cell.Model()) }
        items = Set(a)
    }
    var rows: [[Square]] {
        items.sorted().chunks(ofCount: 2).map { Array($0) }
    }
}

struct DynamicCanvas {
    private var stack = DynamicStack()
    var selected: Square?
    var rows: [[Square]] { stack.rows }
    mutating func select(square: Square) {
        selected = square
    }
    mutating func deselect() {
        guard var item = selected else { return }
        item.uimodel = Cell.Model()
        stack.items.insert(item)
        selected = nil
    }
}

struct RectHoleder {
    var rects = [Square: CGRect]()
    subscript(cell: Square) -> CGRect {
        rects[cell] ?? .zero
    }
}

struct Rects {
    struct Cell: Equatable, Hashable, Comparable, Identifiable {
        let id: Int
        static func < (lhs: Rects.Cell, rhs: Rects.Cell) -> Bool {
            lhs.id < rhs.id
        }
    }
    var rects: [Cell: CGRect] = [:] {
        didSet {
            print("-")
            rects.forEach { print("\($0.key) origin: \($0.value.origin)") }
        }
    }
    subscript(cell: Cell) -> CGRect {
        rects[cell] ?? .zero
    }
}

extension Array: Identifiable where Element == Square {
    public var id: String { reduce(into: "") { $0 += $1.id } }
}
