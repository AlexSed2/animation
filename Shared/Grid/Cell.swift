import SwiftUI
import simd

extension Cell {
    struct Model: Scaleble {
        var diagonal: SIMD2<Float> = [200, 150]
        var offset: SIMD2<Float> = .zero
        var translation: SIMD2<Float> = .zero
        var startTranslation: SIMD2<Float> = .zero
    }
}



struct Cell: View {
    
    @State var model = Model()
    @Binding var globalModel: Grid.Model
    @Binding var globalRectHolder: RectHoleder
    let square: Square
    let parentspace: Namespace.ID
    
    init(square: Square, globalModel: Binding<Grid.Model>, rects: Binding<RectHoleder>, space: Namespace.ID) {
        self.square = square
        self._globalModel = globalModel
        self._globalRectHolder = rects
        parentspace = space
    }
    
    var body: some View {
        
        let drug = DragGesture().onChanged { value in
            var m = model
            m.translation = value.translation.diagonalFloat
            m.startTranslation = value.startLocation.simd2float
            let z = simd_length(model.scaleSimd)
            print("model scale: \(z)")
            if z > 1 {
                globalModel.fridge.select(square: square)
//                if let selected = globalModel.fridge.selected {
//                    if selected != square {
//                        globalModel.fridge.select(square: square)
//                    }
//                }
//                else {
//                    globalModel.fridge.select(square: square)
//                }
            }
            withAnimation(.easeOut) {
                model = m
            }
        }.onEnded { _ in
            withAnimation(Animation.spring()) {


//                if let selected = globalModel.fridge.selected {
//                    if selected == square {
//                        globalModel.fridge.deselect()
//                    } else {
//                        globalModel.fridge.deselect()
//                        var out = square
//                        out.uimodel = model
//                        globalModel.fridge.select(square: out)
//                    }
//                } else {
//                    var out = square
//                    out.uimodel = model
//                    globalModel.fridge.select(square: out)
//                }

                model.translation = .zero
                model.startTranslation = model.diagonal / 2

            }
        }
        
//        let k = Paper(text: square.name)
//            .frame(width: model.size.width, height: model.size.height)
//            .scaleEffect(x: 1 + model.scale.x, y: 1 + model.scale.y, anchor: model.anchor)
//            .gesture(drug)
        
        
        let z = ZStack {
            GeometryReader { geometry in
                check(k: Rectangle().fill(Color.orange), geometry: geometry)
            }
            Paper(text: square.name)
        }.frame(width: model.size.width, height: model.size.height)
            .scaleEffect(x: 1 + model.scale.x, y: 1 + model.scale.y, anchor: model.anchor)
            .gesture(drug)
        
        return z
    }
    
    func check<K: View>(k: K, geometry: GeometryProxy) -> some View {
//        print("key in func: \(square.name), rect: \(geometry.frame(in: .global))")
        DispatchQueue.main.async {
            let rect = geometry.frame(in: .local)
            print("rect: \(rect.origin), size: \(rect.size)")
            globalRectHolder.rects[square] = geometry.frame(in: CoordinateSpace.named(parentspace))
        }
//        globalRectHolder.items.append(square)
        return k
    }
}

struct Paper: View {
    let text: String
    var body: some View {
        ZStack {
            Rectangle().fill(Color.white).opacity(0.8).cornerRadius(10)
            Text(text).foregroundColor(Color.black.opacity(0.8))
        }
    }
}
