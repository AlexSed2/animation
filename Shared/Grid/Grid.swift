import SwiftUI

extension Grid {
    struct Model {
        var fridge = DynamicCanvas()
        var isShown: Bool = false
    }    
}



struct Grid: View {
    
    
    @Namespace var namespace
    @State var model = Model()
    @State var rectHolder = RectHoleder()
    
    var body: some View {
        let v =
        VStack {
            ForEach(model.fridge.rows) { row in
                HStack {
                    ForEach(row) { element in
                        Cell(square: element, globalModel: $model, rects: $rectHolder, space: namespace)
//                            .matchedGeometryEffect(id: element.name, in: namespace)
                    }
                }
            }
        }
//        print("frame: \(rectHolder.rects[cell])")
        let z =
        ZStack {
            v
            if let cell = model.fridge.selected, let frame = rectHolder.rects[cell]
            {
                Rectangle()
                    .fill(Color.orange)
                    .frame(width: frame.width, height: frame.height)
    //                .position(x: frame.origin.x, y: frame.origin.y)
            }
        }
        let s = ScrollView {
            z
        }
        return s
    }
}
