import SwiftUI

protocol Scaleble {
    var diagonal: SIMD2<Float> { get }
    var offset: SIMD2<Float> { get }
    var translation: SIMD2<Float> { get set }
    var startTranslation: SIMD2<Float> { get set }
}

extension Scaleble {
    var scale: (x: CGFloat, y: CGFloat) {
        var k = translation / diagonal
        switch anchor {
        case .bottomTrailing: k = -k
        case .bottomLeading: k.y = -k.y
        case .topTrailing: k.x = -k.x
        default: break
        }
        let cg = k.cgsize
        return (cg.width, cg.height)
    }
    var scaleSimd: SIMD2<Float> {
        [Float(scale.x), Float(scale.y)]
    }
    var size: CGSize { diagonal.cgsize }
    var anchor: UnitPoint {
        let center = diagonal / 2
        let point = startTranslation - center
        let anchor: UnitPoint
        switch (point.x > 0, point.y > 0) {
        case (true, true): anchor = .topLeading
        case (true, false): anchor = .bottomLeading
        case (false, true): anchor = .topTrailing
        case (false, false): anchor = .bottomTrailing
        }
        return anchor
    }
}
