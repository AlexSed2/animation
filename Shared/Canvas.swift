import SwiftUI

struct CanvasGrid: View {
    var body: some View {
        let k =
        ZedGrid()
//        GeometryEffectOne()
//        ExampleTen()
//        AnimationIgnoredByLayout()
//        RotatingCard()
//        ExampleSeven()
//        AnimationCanvas()
//        PrefGrid()
//        TwoCirclesView()
//        GeooetryAndScrollGridView()
//        GridView()
//        Grid()
//        EasyExample()
//        EasyExampleAnchor()
//        EasyExampleTree().padding()
            .frame(minWidth: 20,
                   maxWidth: .infinity,
                   minHeight: 20,
                   maxHeight: .infinity,
                   alignment: .center)
            .background(Rectangle().fill(Color.brown))
            .onAppear {}
        return k
    }
}

struct CanvasGrid_Previews: PreviewProvider {
    static var previews: some View {
        CanvasGrid()
    }
}
