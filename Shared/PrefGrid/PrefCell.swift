import SwiftUI


struct PrefCell: View {
    
    @State var model = CellModel()
    
    var body: some View {
        
        let drug = DragGesture().onChanged { value in
            model.translation = value.translation.diagonalFloat
            model.startTranslation = value.startLocation.simd2float
//            withAnimation(.easeOut) {
//                model.translation = value.translation.diagonalFloat
//                model.startTranslation = value.startLocation.simd2float
//            }
        }.onEnded { _ in
            withAnimation(Animation.spring(response: 0.55, dampingFraction: 0.525, blendDuration: 0.5)) {
                model.translation = .zero
            }
        }
        
        let k = Blank()
            .frame(width: model.size.width, height: model.size.height)
            .anchorPreference(key: CellPreferenceKey.self, value: .bounds, transform: { anchor -> [CellPreferenceData] in
                [CellPreferenceData(viewIdx: 0, scale: model.scale, bounds: anchor, anchor: model.anchor)]
            })
            .scaleEffect(x: 1 + model.scale.x, y: 1 + model.scale.y, anchor: model.anchor)
            .gesture(drug)
        return k
        
    }

}

struct OverlayCell: View {
    
    let size: CGSize = CellModel().diagonal.cgsize
    let scale: (x: CGFloat, y: CGFloat)
    var anchor: UnitPoint
    
    var body: some View {
        let k = Blank()
            .frame(width: size.width, height: size.height)
            .scaleEffect(x: 1 + scale.x, y: 1 + scale.y, anchor: anchor)
        return k
    }

}
