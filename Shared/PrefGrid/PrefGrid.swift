import SwiftUI

struct PrefGrid: View {
 
    var body: some View {
        HStack {
            PrefCell()
            PrefCell()
        }.overlayPreferenceValue(CellPreferenceKey.self) { preferences in
            let first = preferences[1]
            GeometryReader { geometry in
                let rect = geometry[first.bounds]
                OverlayCell(scale: first.scale, anchor: .topLeading).offset(x: rect.origin.x + 10, y: rect.origin.y + 10)
            }
        }
    }
    
}
