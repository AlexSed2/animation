import SwiftUI
import simd

struct CellModel: Scaleble {
    var diagonal: SIMD2<Float> = [200, 150]
    var offset: SIMD2<Float> = .zero
    var translation: SIMD2<Float> = .zero
    var startTranslation: SIMD2<Float> = .zero
}

struct CellPreferenceData: Equatable {
    static func == (lhs: CellPreferenceData, rhs: CellPreferenceData) -> Bool {
        lhs.bounds == rhs.bounds
    }
    
    let viewIdx: Int
    let scale: (x: CGFloat, y: CGFloat)
    let bounds: Anchor<CGRect>
    let anchor: UnitPoint
}

struct CellPreferenceKey: PreferenceKey {
    static var defaultValue: [CellPreferenceData] = []
    static func reduce(value: inout [CellPreferenceData], nextValue: () -> [CellPreferenceData]) {
        value.append(contentsOf: nextValue())
    }
}
