import CoreGraphics

struct GridModel: Scaleble {
    let diagonal: SIMD2<Float> = [200, 150]
    var offset: SIMD2<Float> = .zero
    var translation: SIMD2<Float> = .zero
    var startTranslation: SIMD2<Float> = .zero
}
