import SwiftUI

struct GridView: View {
    
    struct Model {
        var patent: SIMD2<Float> = .zero
        var isShown: Bool = false
    }
    
    @Namespace var namespace
    @State var inspector = Model()
    
    var body: some View {
        
        let m =
        HStack {
            if !inspector.isShown {
                ScaleCellView(shown: $inspector.isShown).matchedGeometryEffect(id: "Rect", in: namespace).onTapGesture {
                    withAnimation(.spring(response: 0.5, dampingFraction: 0.7)) {
                        inspector.isShown.toggle()
                    }
                }
            }
            
            Geometry(shown: $inspector.isShown)
        }
        
        let k =
        HStack {
            ScaleCellView(shown: .constant(false))
            ScaleCellView(shown: .constant(false))
        }
        
        let v =
        VStack {
            m
            k
        }
        
        let z =
        ZStack {
            v
            if inspector.isShown {
                ScaleCellView(shown: $inspector.isShown).matchedGeometryEffect(id: "Rect", in: namespace).onTapGesture {
                    withAnimation(.spring(response: 0.5, dampingFraction: 0.7)) {
                        inspector.isShown.toggle()
                    }
                }
            }
        }
        
        
        return z
    }
}

struct Geometry: View {
    @State var model = GridModel()
    @Binding var shown: Bool
    var body: some View {
        let r = Rectangle()
       
        let g = GeometryReader { geometry in
            let d = geometry.size.diagonalFloat / 2
            let s = d.cgsize
            let rect = CGRect(origin: .zero, size: s)
            r.path(in: rect).fill(Color.gray)
        }
        let z =
        ZStack {
            r.fill(Color.indigo)
            g
        }//.cornerRadius(10)
        
        let drug = DragGesture().onChanged { value in
            withAnimation(.easeOut) {
                model.translation = value.translation.diagonalFloat
                model.startTranslation = value.startLocation.simd2float
            }
        }.onEnded { _ in
            withAnimation(Animation.spring()) {
                model.translation = .zero
            }
        }
        
        let k = z
            .frame(width: model.size.width, height: model.size.height)
            .scaleEffect(x: 1 + model.scale.x, y: 1 + model.scale.y, anchor: model.anchor)
            .gesture(drug)
            .onTouchDownGesture {
                print("touchDown")
            }
        
        return k
    }
}

extension View {
    func onTouchDownGesture(callback: @escaping () -> Void) -> some View {
        modifier(OnTouchDownGestureModifier(callback: callback))
    }
}

private struct OnTouchDownGestureModifier: ViewModifier {
    @State private var tapped = false
    let callback: () -> Void

    func body(content: Content) -> some View {
        content
            .simultaneousGesture(DragGesture(minimumDistance: 0)
                .onChanged { _ in
                    if !self.tapped {
                        self.tapped = true
                        self.callback()
                    }
                }
                .onEnded { _ in
                    self.tapped = false
                })
    }
}
