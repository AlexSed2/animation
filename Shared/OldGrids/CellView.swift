import SwiftUI

struct Canvas: View {
    var body: some View {
        let k =
        ScaleCellView(shown: .constant(false))
            .foregroundColor(Color.orange)
            .frame(minWidth: 400,
                           maxWidth: .infinity,
                           minHeight: 100,
                           maxHeight: 300,
                           alignment: .center)
            .background(Rectangle().fill(Color.brown))
            .onAppear {}
        return k
    }
}

struct ScaleCellView: View {
    
    @State var model = CellModel()
    @Binding var shown: Bool
    
    var body: some View {
        
        let drug = DragGesture().onChanged { value in
            withAnimation(.easeOut) {
                model.translation = value.translation.diagonalFloat
                model.startTranslation = value.startLocation.simd2float
            }
        }.onEnded { _ in
            withAnimation(Animation.spring()) {
                model.translation = .zero
                withAnimation {
                    shown.toggle()
                }
            }
        }
        
        let k = Blank()
            .frame(width: model.size.width, height: model.size.height)
            .scaleEffect(x: 1 + model.scale.x, y: 1 + model.scale.y, anchor: model.anchor)
            .gesture(drug)
        return k
    }
}

struct Blank: View {
    var body: some View {
        ZStack {
            Rectangle().fill(Color.white).opacity(0.8).cornerRadius(10)
            Text("Hello").foregroundColor(Color.black.opacity(0.8))
        }
    }
}

