import SwiftUI

struct Explore<Z: Animatable & VectorArithmetic> {
    let k: Z
    func run() {
        let k = Capsule()
        anim(a: k)
    }
    func add<M: VectorArithmetic>(m: M) {
        var m = m
        m.scale(by: 2)
    }
    func anim<A: Animatable>(a: A){
        let k = a.animatableData
        print(k)
    }
    func transaction() {
        let k = Transaction()
        let n = Namespace()
        
    }
    func anchor() {
        let g = GeometryReader { geometry in
        }.offset(x: 0, y: 0)
    }
    func geom<E: GeometryEffect>(e: E) {
    }
}
