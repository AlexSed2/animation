import SwiftUI

struct TwoCirclesView : View {
    @State private var draggingLocation = CGPoint.zero
    @State private var startLocation = CGPoint.zero
    @State private var dragging = false

    var body: some View {
        let GR = DragGesture(minimumDistance: 10, coordinateSpace: .named("myCoordinateSpace"))
            .onEnded { value in
                self.dragging = false
                self.draggingLocation = CGPoint.zero
                self.startLocation = CGPoint.zero
        }
        .onChanged { value in
            if !self.dragging {
                self.dragging = true
            }

            if self.startLocation == CGPoint.zero {
                self.startLocation = value.startLocation
            }
            self.draggingLocation = value.location
        }

        return ZStack(alignment: .topLeading) {

            Circle()
                .fill(self.dragging ? Color.blue : Color.red)
                .frame(width: 100, height: 100)
                .overlay(Text("Circle 1"))
                .gesture(GR)
                .offset(x: 75, y: 75)

            Circle()
                .fill(self.dragging ? Color.blue : Color.red)
                .frame(width: 100, height: 100)
                .overlay(Text("Circle 2"))
                .gesture(GR)
                .offset(x: 200, y: 200)

            if self.dragging {
                Path { path in
                    path.move(to: CGPoint(x: self.startLocation.x-5, y: self.startLocation.y-5))
                    path.addLine(to: CGPoint(x: self.draggingLocation.x-5, y: self.draggingLocation.y+5))
                    path.addLine(to: CGPoint(x: self.draggingLocation.x+5, y: self.draggingLocation.y-5))
                    path.addLine(to: CGPoint(x: self.startLocation.x+5, y: self.startLocation.y+5))
                }
                .fill(Color.black)
            }

        }
        .coordinateSpace(name: "myCoordinateSpace")
        .frame(width: 400, height: 400, alignment: .topLeading)
        .background(Color.gray)
    }
}
