import SwiftUI

struct ContactBuilder: View {
    @State private var username: String = ""
    var font: Font {
        Font.system(size: 30, weight: .medium, design: .default)
    }
    var body: some View {
        
        TextField(text: $username, prompt: Text("Required")) { Text("Username") }

        .frame(width: 200, height: 100, alignment: .center)
        .background(Rectangle().fill(Color.orange)).font(font)

    }
}
