import SwiftUI

struct GeometryEffectOne: View {
    @State private var flag: Bool = true
    @Namespace var namespace
    
    var body: some View {
        HStack {
            
            if flag {
                Rectangle().fill(Color.green)
                    .matchedGeometryEffect(id: 1, in: namespace)
                    .frame(width: 100, height: 100)
            }
            
            Spacer()
            
            Button("Switch") {
                
                withAnimation(.easeInOut(duration: 2.0)) {
                    flag.toggle()                    
                }
                
            }
            
            Spacer()
            
            VStack {
                Rectangle().fill(Color.yellow).frame(width: 50, height: 50)
                
                if !flag {
                    Circle()
                        .fill(Color.blue)
                        .matchedGeometryEffect(id: 1, in: namespace)
                        .border(Color.black)
                        .frame(width: 50, height: 50)
                        .zIndex(1)
                }
                
                Rectangle().fill(Color.yellow).frame(width: 60, height: 60)
            }
            
        }.frame(width: 400)
    }
}
