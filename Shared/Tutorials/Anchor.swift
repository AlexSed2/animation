import SwiftUI

struct AnchorPreferenceData: Equatable {
    let viewIdx: Int
    let bounds: Anchor<CGRect>
}

struct AnchorPreferenceKey: PreferenceKey {
    static var defaultValue: [AnchorPreferenceData] = []
    static func reduce(value: inout [AnchorPreferenceData], nextValue: () -> [AnchorPreferenceData]) {
        value.append(contentsOf: nextValue())
    }
}

struct MonthViewAnchor: View {
    @Binding var activeMonth: Int
    let label: String
    let idx: Int
    
    var body: some View {
        Text(label)
            .padding(10)
            .anchorPreference(key: AnchorPreferenceKey.self, value: .bounds, transform: { anchor -> [AnchorPreferenceData] in
                [AnchorPreferenceData(viewIdx: idx, bounds: anchor)]
            })
            .onTapGesture { self.activeMonth = self.idx }
    }
}

struct EasyExampleAnchor: View {
    
    @State private var activeIdx: Int = 0
    
    var body: some View {
        VStack {
            Spacer()
            
            HStack {
                MonthViewAnchor(activeMonth: $activeIdx, label: "January", idx: 0)
                MonthViewAnchor(activeMonth: $activeIdx, label: "February", idx: 1)
                MonthViewAnchor(activeMonth: $activeIdx, label: "March", idx: 2)
                MonthViewAnchor(activeMonth: $activeIdx, label: "April", idx: 3)
            }
            
            Spacer()
            
            HStack {
                MonthViewAnchor(activeMonth: $activeIdx, label: "May", idx: 4)
                MonthViewAnchor(activeMonth: $activeIdx, label: "June", idx: 5)
                MonthViewAnchor(activeMonth: $activeIdx, label: "July", idx: 6)
                MonthViewAnchor(activeMonth: $activeIdx, label: "August", idx: 7)
            }
            
            Spacer()
            
            HStack {
                MonthViewAnchor(activeMonth: $activeIdx, label: "September", idx: 8)
                MonthViewAnchor(activeMonth: $activeIdx, label: "October", idx: 9)
                MonthViewAnchor(activeMonth: $activeIdx, label: "November", idx: 10)
                MonthViewAnchor(activeMonth: $activeIdx, label: "December", idx: 11)
            }
            
            Spacer()
        }.backgroundPreferenceValue(AnchorPreferenceKey.self) { preferences in
            GeometryReader { geometry in
                createBorder(geometry, preferences)
            }
        }
    }
    
    func createBorder(_ geometry: GeometryProxy, _ preferences: [AnchorPreferenceData]) -> some View {
        
        let p = preferences.first(where: { $0.viewIdx == self.activeIdx })
        
        let bounds = p != nil ? geometry[p!.bounds] : .zero
                
        return RoundedRectangle(cornerRadius: 15)
                .stroke(lineWidth: 3.0)
                .foregroundColor(Color.green)
                .frame(width: bounds.size.width, height: bounds.size.height)
                .fixedSize()
                .offset(x: bounds.minX, y: bounds.minY)
                .animation(.easeInOut(duration: 3.0), value: activeIdx)
    }
}
