import SwiftUI

func getView(number: Int, flipped: Bool) -> some View {
    print("number: \(number), fliped: \(flipped)")
    return ZStack {
            switch (number % 5, flipped) {
            case (0, false): Capsule().fill(Color.red)
            case (1, false): Rectangle().fill(Color.gray)
            case (2, false): Circle().fill(Color.orange)
            case (3, false): PolygonShape(sides: 8, scale: 0.5).fill(Color.green)
            case (_ ,true): RoundedRectangle(cornerSize: CGSize(width: 20, height: 20)).fill(Color.white.opacity(0.8))
            default: Text("Hello").scaleEffect(5)
        }
    }
}


struct RotatingCard: View {
    @State private var flipped = Flipped()
    @State private var animate3d = false
    @State private var rotate = false
    @State private var imgIndex = 0
    
    struct Flipped {
        var now: Bool = false
        var prev: Bool = false
    }
    
    var body: some View {
    
        if flipped.now != flipped.prev {
            DispatchQueue.main.async {
                flipped.prev = flipped.now
                imgIndex += 1
            }
        }
        
        print("rotate: \(rotate)")
   
        let v =
        VStack {
            Spacer()
            getView(number: imgIndex, flipped: flipped.now)
                .frame(width: 265, height: 400)
                .modifier(FlipEffect(flipped: $flipped.now, angle: animate3d ? 360 : 0, axis: (x: 1, y: 5)))
                .rotationEffect(Angle(degrees: rotate ? 0 : 360))
                .onAppear {
                    withAnimation(Animation.linear(duration: 4.0).repeatForever(autoreverses: false)) {
                        animate3d = true
                    }

                    withAnimation(Animation.linear(duration: 8.0).repeatForever(autoreverses: false)) {
                        rotate = true
                    }
                }
            Spacer()
        }
        return v
    }
    
}

struct FlipEffect: GeometryEffect {
    
    var animatableData: Double {
        get { angle }
        set { angle = newValue }
    }
    
    @Binding var flipped: Bool
    
    var angle: Double
    let axis: (x: CGFloat, y: CGFloat)
    
    func effectValue(size: CGSize) -> ProjectionTransform {
        
        // We schedule the change to be done after the view has finished drawing,
        // otherwise, we would receive a runtime error, indicating we are changing
        // the state while the view is being drawn.
        DispatchQueue.main.async {
            self.flipped = self.angle >= 90 && self.angle < 270
        }
        
        let a = CGFloat(Angle(degrees: angle).radians)
        
        var transform3d = CATransform3DIdentity
        transform3d.m34 = -1 / max(size.width, size.height)
        
        transform3d = CATransform3DRotate(transform3d, a, axis.x, axis.y, 0)
        transform3d = CATransform3DTranslate(transform3d, -size.width/2.0, -size.height/2.0, 0)
        
        let affineTransform = ProjectionTransform(CGAffineTransform(translationX: size.width/2.0, y: size.height / 2.0))
        
        return ProjectionTransform(transform3d).concatenating(affineTransform)
    }
}

struct AnimationIgnoredByLayout: View {
    @State private var animate = false
    
    var body: some View {
        VStack {
            RoundedRectangle(cornerRadius: 5)
                .foregroundColor(.green)
                .frame(width: 300, height: 50)
                .overlay(ShowSize())
                .modifier(MyEffect(x: animate ? -10 : 10))
            
            RoundedRectangle(cornerRadius: 5)
                .foregroundColor(.blue)
                .frame(width: 300, height: 50)
                .overlay(ShowSize())
                .modifier(MyEffect(x: animate ? 10 : -10).ignoredByLayout())
            
        }.onAppear {
            withAnimation(Animation.easeInOut(duration: 1.0).repeatForever()) {
                self.animate = true
            }
        }
    }
}

struct MyEffect: GeometryEffect {
    var x: CGFloat = 0
    
    var animatableData: CGFloat {
        get { x }
        set { x = newValue }
    }
    
    func effectValue(size: CGSize) -> ProjectionTransform {
        return ProjectionTransform(CGAffineTransform(translationX: x, y: 0))
    }
}

struct ShowSize: View {
    var body: some View {
        GeometryReader { proxy in
            Text("x: \(proxy.frame(in: .global).origin.x)")
                .foregroundColor(.white)
        }
    }
}
