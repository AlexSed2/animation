import SwiftUI

struct AnimationCanvas: View {
    var body: some View {
        ExampleFour()
    }
}

struct ExampleOne: View {
    @State var half = false
    @State var dim = false
    
    var body: some View {
        Rectangle().fill(Color.white.opacity(0.8)).frame(width: 100, height: 100)
            .scaleEffect(half ? 0.5 : 1.0)
            .opacity(dim ? 0.2 : 1.0)
            .animation(.easeInOut(duration: 1.0), value: dim)
            .onTapGesture {
                dim.toggle()
                half.toggle()
            }
    }
}

struct ExampleTwo: View {
    @State var half = false
    @State var dim = false
    
    var body: some View {
        Rectangle().fill(Color.white.opacity(0.8)).frame(width: 100, height: 100)
            .scaleEffect(half ? 0.5 : 1.0)
            .opacity(dim ? 0.5 : 1.0)
            .onTapGesture {
                half.toggle()
                withAnimation(.easeInOut(duration: 1.0)) {
                    dim.toggle()
                }
        }
    }
}

struct ExampleThree: View {
    @State private var half = false
    @State private var dim = false
    
    var body: some View {
        Rectangle().fill(Color.white.opacity(0.8)).frame(width: 100, height: 100)
            .opacity(dim ? 0.2 : 1.0)
            .animation(.easeInOut(duration: 1.0), value: dim)
            .scaleEffect(half ? 0.5 : 1.0)
            .onTapGesture {
                self.dim.toggle()
                self.half.toggle()
            }
    }
}

struct ExampleFour: View {
    @State var sides: Int = 3
    @State var scale: Double = 1
    var body: some View {
        PolygonShape(sides: sides, scale: scale).stroke(lineWidth: 10).fill(Color.white.opacity(0.5))
            .animation(.spring(), value: sides)
            .onTapGesture {
                sides += 1
                scale *= 0.9
            }
    }
}
