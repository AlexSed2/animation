import SwiftUI

struct SkewedOffset: GeometryEffect {
    var offset: CGFloat
    var pct: CGFloat
    let goingRight: Bool

    init(offset: CGFloat, pct: CGFloat, goingRight: Bool) {
        self.offset = offset
        self.pct = pct
        self.goingRight = goingRight
    }

    var animatableData: AnimatablePair<CGFloat, CGFloat> {
        get { return AnimatablePair<CGFloat, CGFloat>(offset, pct) }
        set {
            offset = newValue.first
            pct = newValue.second
        }
    }

    func effectValue(size: CGSize) -> ProjectionTransform {
        var skew: CGFloat

        if pct < 0.2 {
            skew = (pct * 5) * 0.5 * (goingRight ? -1 : 1)
        } else if pct > 0.8 {
            skew = ((1 - pct) * 5) * 0.5 * (goingRight ? -1 : 1)
        } else {
            skew = 0.5 * (goingRight ? -1 : 1)
        }

        return ProjectionTransform(CGAffineTransform(a: 1, b: 0, c: skew, d: 1, tx: offset, ty: 0))
    }
}

struct Car: View {
    var offset: CGFloat
    var pct: CGFloat
    var body: some View {
        let z =
        Rectangle()
            .fill(Color.white.opacity(0.8))
            .frame(width: 200, height: 100)
            .modifier(SkewedOffset(offset: offset, pct: pct, goingRight: offset > 0))
        return z
    }
}

struct ExampleSeven: View {
    @State private var moveIt = false

    var body: some View {
        let animation = Animation.easeInOut(duration: 1.0)
        Car(offset: moveIt ? -100 : 100, pct: moveIt ? 1 : 0)
            .onTapGesture {
                withAnimation(animation.delay(0.6)) {
                    moveIt.toggle()
                }
            }
    }
}

struct DummyAnimationView: View {
    var body: some View {
        let z = Car(offset: 1, pct: 1)
        return z
    }
}
